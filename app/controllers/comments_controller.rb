class CommentsController < ApplicationController
  before_action :authenticate_user!, except: [:get_post]
  before_action :get_post, only: [:create, :destroy]

  def get_post
    @post = Post.find(params[:post_id])
  end

  def create
    @comment = @post.comments.create(params[:comment].permit(:name, :body))

    redirect_to post_path(@post)
  end

  def destroy
    @comment = @post.comments.find(params[:id])
    @comment.destroy

    redirect_to post_path(@post)
  end


end
